<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class JobController extends Controller
{
    const url='http://laravelhorizon/public/';
    public function index()
    {
        $client=new Client();
        $response=$client->request('GET',self::url.'index', [
//            'headers'=>[
//                "x-rapidapi-key"=> "undefined",
//                "x-rapidapi-host"=> "wordsapiv1.p.rapidapi.com",
//                "useQueryString"=> true
//            ],
            'body'=>'hello world',
        ] );
        return $response->getBody();
    }

    public function response(Request $request)
    {
        $file=fopen('response.txt','w');
        fwrite($file, $request->getContent());
        fclose($file);
    }

    public function responseSync(Request $request)
    {
        $file=fopen('responseSync.txt','w');
        fwrite($file, $request->getContent());
        fclose($file);
    }

    public function sync()
    {
        $client=new Client();
        $response=$client->request('GET',self::url.'sync', [
//            'headers'=>[
//                "x-rapidapi-key"=> "undefined",
//                "x-rapidapi-host"=> "wordsapiv1.p.rapidapi.com",
//                "useQueryString"=> true
//            ],
            'body'=>'hello worlds',
        ] );
        return $response->getBody();
    }

}
