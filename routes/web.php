<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', 'App\Http\Controllers\JobController@index')->name('index');
Route::get('/sync', 'App\Http\Controllers\JobController@sync')->name('sync');
Route::get('/response', 'App\Http\Controllers\JobController@response')->name('response');
Route::get('/responseSync', 'App\Http\Controllers\JobController@responseSync')->name('response');